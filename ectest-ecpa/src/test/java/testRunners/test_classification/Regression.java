package testRunners.test_classification;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.ec.selenium.core.testbase.Hooks;

import cucumber.api.CucumberOptions;
import cucumber.runtime.arquillian.CukeSpace;

@RunWith(CukeSpace.class)
@CucumberOptions(
        tags={"@regression"},
        plugin={"pretty", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/Regression.html", "json:target/cucumber/Regression.json", "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"},
        monochrome=true,
        features={"src/test/features"}, 
        glue = {"com.ec"},
        strict=true)

public class Regression {
    @Drone
    private WebDriver driver;
    @BeforeClass
    public static void setUp() {
        Hooks.csvReport("Regression");
    }
}
