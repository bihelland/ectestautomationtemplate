package testRunners.test_classification;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.ec.selenium.core.testbase.Hooks;

import cucumber.api.CucumberOptions;
import cucumber.runtime.arquillian.CukeSpace;

@RunWith(CukeSpace.class)
@CucumberOptions(
        tags={"@sanity"},
        plugin={"pretty", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/Sanity.html", "json:target/cucumber/Sanity.json", "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"},
        monochrome=true,
        features={"src/test/features"}, 
        glue = {"com.ec.revenue.financialTransaction.storysteps"},
        strict=true)

public class Sanity {
    @Drone
    private WebDriver driver;
    
    @BeforeClass
    public static void setUp() {
        Hooks.csvReport("Sanity");
    }
}
