package testRunners;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.ec.selenium.core.testbase.Hooks;

import cucumber.api.CucumberOptions;
import cucumber.runtime.arquillian.CukeSpace;

@RunWith(CukeSpace.class)
@CucumberOptions(
        tags={"@demo"},
        plugin={"pretty", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/Demo.html", "json:target/cucumber/Demo.json", "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"},
        monochrome=true,
        features={"src/test/features"}, 
        glue = {"com.ec"},
        strict=true)

public class Demo {
    @Drone
    private WebDriver driver;
    
    @BeforeClass
    public static void setUp() {
        Hooks.csvReport("Demo");
    }
}
