@demo
Feature: Demo feature which include different cucumber functionality

  Background:
    Given I login with "sysadmin" user and "xn47GwcKwUt4Y+Sjx906UQ==" as encrypted password

  @scenarioOutline
  Scenario Outline: Test to perform create, modify and delete operation on User Maintenance screen
    When I navigate to "User Maintenance" screen and enter navigation data
      | User Group |
      | Default    |
    And I create "USER" in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   | Active   |
      | <user_id> | <surname> | <given_name> | <application> | <active> |
    Then I verify following "USER" is present in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   |
      | <user_id> | <surname> | <given_name> | <application> |
    When I select given "USER" in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   |
      | <user_id> | <surname> | <given_name> | <application> |
    And I modify "USER" in TABLE on "User Maintenance" screen
      | Title   |
      | <title> |
    Then I verify following "USER" is present in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   | Title   |
      | <user_id> | <surname> | <given_name> | <application> | <title> |
    When I create "ROLE" in TABLE on "User Maintenance" screen
      | Role   |
      | <role> |
    Then I verify following "ROLE" is present in TABLE on "User Maintenance" screen
      | User      | Role   |
      | <user_id> | <role> |
    And I delete "ROLE" in TABLE on "User Maintenance" screen
      | User      | Role   |
      | <user_id> | <role> |
    When I select given "USER" in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   | Title   |
      | <user_id> | <surname> | <given_name> | <application> | <title> |
    And I delete "USER" in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   | Title   |
      | <user_id> | <surname> | <given_name> | <application> | <title> |
    Then I verify following "USER" is not present in TABLE on "User Maintenance" screen
      | User Id   | Surname   | Given Name   | Application   | Title   |
      | <user_id> | <surname> | <given_name> | <application> | <title> |
    And I logout from application
    Examples:
      | user_id    | surname       | given_name | application | active | title        | role                 |
      | testuser-1 | testSurname-1 | user-1     | EC          | true   | Demo Title 1 | System Administrator |
      | testuser-2 | testSurname-2 | user-2     | EC          | true   | Demo Title 2 | Operator             |
      | testuser-3 | testSurname-3 | user-3     | EC          | true   | Demo Title 3 | Terminal Operator    |
      | testuser-4 | testSurname-4 | user-4     | EC          | true   | Demo Title 4 | Supervisor           |

  @scenario
  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module
    When I navigate to "Harbour Interruption" screen and enter navigation data
      | From Date  | To Date    | Production Unit | Area     | Facility Class 1 |
      | 2011-01-01 | 2011-01-10 | TS1_EC_PU       | TS1_Area | TS1 Fcty         |
    And I create "CARGO LOADING DELAY" in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Test     |
    Then I verify following "CARGO LOADING DELAY" is present in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Test     |
    When I select given "CARGO LOADING DELAY" in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Test     |
    And I modify "CARGO LOADING DELAY" in TABLE on "Harbour Interruption" screen
      | Comments    |
      | Testing@123 |
    Then I verify following "CARGO LOADING DELAY" is present in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments    |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Testing@123 |
    When I delete "CARGO LOADING DELAY" in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments    |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Testing@123 |
    Then I verify following "CARGO LOADING DELAY" is not present in TABLE on "Harbour Interruption" screen
      | Delay From       | Delay To         | Reason      | Comments    |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | Strong wind | Testing@123 |
    And I logout from application