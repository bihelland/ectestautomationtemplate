package com.ec.selenium.ecpages.transport.terminaloperation;

import com.ec.selenium.core.screenlet.TableScreenlet;
import com.ec.selenium.pageutils.PageComponents;

public class HarbourInterruptionPage extends PageComponents {
    public static final String MENU_CARGO_LOADING_DELAY = "CARGO LOADING DELAY";
    public static final String LOADING_DELAY_T = "loadingDelay";
    TableScreenlet cargoLoadingDelayTablescreenlet = null;

    public HarbourInterruptionPage() {
        location = getNavPath("TRAN_HARBOUR_INTERRUPTION");
    }

    public TableScreenlet getLoadingDelayTable() {
        return (cargoLoadingDelayTablescreenlet = cargoLoadingDelayTablescreenlet == null ? getTableScreenlet(LOADING_DELAY_T) : cargoLoadingDelayTablescreenlet);
    }
}