package com.ec.auto.customcore;

import com.ec.selenium.util.EncryptDecryptText;
import org.apache.log4j.Logger;

public class EncryptDecryptUtility {

    private static final Logger log = Logger.getLogger(EncryptDecryptUtility.class);

    public static void main(String args[]) throws Exception {
        EncryptDecryptText encryptDecryptText = new EncryptDecryptText();
        // Use below code to encrypt given text. For example here we are encrypting text as 'Test'
        String encryptedText = encryptDecryptText.encrypt("Test");
        log.info("Encrypted text:=>" + encryptedText);

        // Use below code to decrypt given encrypted text. For example we have used below encrypted text as 'Test' to decrypt
        String decryptedText = encryptDecryptText.decrypt("Dr3oziHDS84=");
        log.info("Decrypted text:=>" + decryptedText);
    }
}
