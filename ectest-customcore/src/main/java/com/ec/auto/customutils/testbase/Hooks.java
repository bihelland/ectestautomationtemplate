package com.ec.auto.customutils.testbase;

import java.io.File;

import org.apache.log4j.Logger;

import com.cucumber.listener.Reporter;
import com.ec.selenium.util.TestUtil;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    private static final Logger log = Logger.getLogger(Hooks.class);

    /**
     * This method would run after @Before(order=0) present under Hooks.java
     *
     * @param scenario
     */
    @Before(order = 1)
    public void beforeScenario(Scenario scenario) {
       // Perform actions before start of each scenario
    }

    /**
     * This method would run before @After(order=0) present under Hooks.java
     *
     * @param scenario
     */
    @After(order = 1)
    public void afterScenario(Scenario scenario) {
        addCustomExtentReportsParameters();
    }
    
    public void addCustomExtentReportsParameters(){
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
        Reporter.setSystemInfo("EC APP URL", TestUtil.getHost());
        Reporter.setSystemInfo("OS", System.getProperty("os.name"));
        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
    }
}
