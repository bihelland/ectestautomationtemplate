# README #

### Test Automation ###
*	This README will help to set up 'Test Automation Toolkit' & execute tests on the local machine

### Prerequisites Setup ###

##### Setup Java #####

*	Download and install JDK 11 or OpenJdk 11
*	Go to System Properties >> Advanced >> Environment Variables >> System Variables >> Create variable 'JAVA_HOME' with local JDK path as value (e.g. C:\Program Files\Java\jdkVersion)
*	Edit 'PATH' variable >> Add %JAVA_HOME%\bin to the existing value 
*	Verify whether Java is correctly set up >> command prompt >> java -version >> Should show currently installed Java version  
	
##### Setup Maven #####

*	Download or copy maven zip file and extract it (**For example:** extract under C:\Maven)
*	Go to System Properties >> Advanced >> Environment Variables >> System Variables >> Create variable 'M2_HOME' with value C:\Maven
*	Edit 'PATH' variable >> Add %M2_HOME%\bin to the existing value
*	Verify whether Maven is correctly set up >> command prompt >> mvn -version >> Should show correct Maven version

##### Setup Automation Toolkit locally #####

*	Clone/Copy AutomationToolkit_x_xx.zip file from repository or shared folder and extract it, which contains 2 folders 'Toolkit' and 'Jars' 
*	Go into 'Jars' folder. Open command prompt and execute below commands for installing ectest-core jar and ectest-testagent war to local maven:
      * mvn install:install-file -Dfile=ectest-core-12.2.10.jar -DgroupId=com.ec.test -DartifactId=ectest-core -Dversion=12.2.10 -Dpackaging=jar
      * mvn install:install-file -Dfile=ectest-testagent-12.2.10.war -DgroupId=com.ec.test -DartifactId=ectest-testagent -Dversion=12.2.10 -Dpackaging=war
*	Go into 'Toolkit\ectestautomation' folder. Open command prompt and execute below command:
      * mvn clean install -DskipTests  (**Note:** Build should be successful)

##### Setup Driver #####

*	Download & extract chromedrive.exe from [here](https://chromedriver.chromium.org/downloads) (**Note:** Make sure always Chrome browser and Chrome drive exe file should be sync. They should be compatible to each other)
	  
##### Deploy ectest-testagent #####

*	Make sure ectest-testagent war file has to be deployed under ec-server-group on wildfly window or through using command 
      * War file path: AutomationToolkit_x.x.x_xxxx\Jars\ectest-testagent-12.2.10.war

##### Setup Eclipse #####

*	Download/Copy Eclipse and install it
*	Open Eclipse >> File >> Import.. >> Maven >> Existing Maven Project >> Browse >> Select parent pom.xml (Toolkit\ectestautomation\pom.xml)
*	Go to Eclipse >> Help >> Eclipse MarketPlace. Enter 'cucumber' in search box and click on 'Go' button. Install 'Cucumber Eclipse plugin' and 'Natural' plugin

##### Setup IntelliJ IDEA #####

*   Download/Copy Intellij Idea and install it
*   Open IntelliJ and provide project workspace path and Open project.
*   Import changes in maven project.

### Execute UI Tests ###

##### Using Eclipse #####

*	Open required test class (from \ectest-ui\src\test\java)
*	Go to Ecplipse >> Run >> Run Configurations >> JUnit >> verify your test class name from 'Test' tab. Go to 'Arguments' tab and pass VM arguments as "-Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port>". Click on 'Apply' button, then 'Run' button to execute test

##### Using Maven #####

*	Go into 'Toolkit\ectestautomation' folder. Open command prompt and hit below command to execute test as per requirement:
*	To execute all UI tests:
      * mvn -pl ectest-ui -DskipITs=false -DfailIfNoTests=false -Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port> verify

*	To execute all tests from a particular group, which is marked as @Category (e.g: IUD): 
      * mvn -pl ectest-ui -DskipITs=false -DfailIfNoTests=false -Dgroups="com.ec.categories.IUD" -Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port> verify

*	To execute specific test (e.g: DbRestHelperTest):
      * mvn -pl ectest-ui -DskipITs=false -DfailIfNoTests=false -DtestInclude="com.ec.demo.iud.dbresthelper.DbRestHelperTest" -Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port> verify
  
### Execute ECPA Tests ###

##### Using Eclipse #####

*	Open required .feature file present under "\ectest-ecpa\src\test\features" folder
*	Copy tag (e.g. @demo) present in that feature file
*	Open any test runner from "\ectest-ecpa\src\test\java\testRunners' (e.g. Demo.java)
*	Specify copied tag in tags={"@demo"}
*	Go to Ecplipse >> Run >> Run Configurations >> JUnit >> verify your test class name from 'Test' tab. Go to 'Arguments' tab and pass VM arguments as "-Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port>". Click on 'Apply' button, then 'Run' button to execute test
	
##### Using Maven #####

*	Open required .feature file present under "\ectest-ecpa\src\test\features" folder
*	Copy tag (e.g. @demo) present in that feature file
*	Open any test runner from "\ectest-ecpa\src\test\java\testRunners' (e.g. Demo.java)
*	Specify copied tag in tags={"@demo"}
*	Go into 'Toolkit\ectestautomation' folder. Open command prompt and hit below command to execute test: 
      * mvn -pl ectest-ecpa -DskipITs=false -DtestInclude=Demo -Dselenium.chromedriverpath="<chrome_drive_exe_filepath>" -Dgridhubaddress=http://localhost:4444 -DEC_APP_URL=<ec_application_url:port> verify

### Results and Screenshots ###

##### Results #####

*	Extent Report:
      * UI Test : Toolkit\ectestautomation\ectest-ui\target\extent_report.html
      * ECPA Test : Toolkit\ectestautomation\ectest-ecpa\target\cucumber\<TestRunnerName.html>
	  
*	CSV Report:
      * UI Test : Toolkit\ectestautomation\ectest-ui\target\UITestResults.csv
      * ECPA Test : Toolkit\ectestautomation\ectest-ecpa\target\<TestRunnerName.csv>
	  
*	To generate Allure Report: (Currently in progress)
      * Go into 'Toolkit\ectestautomation' folder. Open command prompt and run command: mvn allure:serve
	  
##### Screenshots #####

      * UI Test : Toolkit\ectestautomation\ectest-ui\target\screenshots
      * ECPA Test : Toolkit\ectestautomation\ectest-ecpa\target\screenshots
