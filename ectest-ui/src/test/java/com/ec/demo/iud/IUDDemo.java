package com.ec.demo.iud;

import java.util.HashMap;
import java.util.Map;

import org.jboss.arquillian.graphene.page.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.ec.categories.IUD;
import com.ec.selenium.core.screenlet.TableScreenlet;
import com.ec.selenium.core.testbase.UITestBase;
import com.ec.selenium.ecpages.transport.terminaloperation.HarbourInterruptionPage;

/**
 * @author chhabshw
 */
@Category({IUD.class})
@RunWith(Arquillian.class)
public class IUDDemo extends UITestBase {

    @Page
    private HarbourInterruptionPage harbourInterruption;

    @Test
    public void IUDTest() {
        harbourInterruption.load("sysadmin", "sysadmin");
        //Navigate to screen
        Map<String, String> navMap = new HashMap<>();
        navMap.put("From Date", "2011-01-01");
        navMap.put("To Date", "2011-01-10");
        navMap.put("Production Unit", "TS1_EC_PU");
        navMap.put("Area", "TS1_Area");
        navMap.put("Facility Class 1", "TS1 Fcty");
        harbourInterruption.navSearchByECLabel(navMap);

        //Insert record on screen
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("Delay From", "2011-01-01 00:00");
        dataMap.put("Delay To", "2011-01-02 00:00");
        dataMap.put("Reason", "Strong wind");
        dataMap.put("Comments", "Test01");
        TableScreenlet harbourTable = harbourInterruption.getLoadingDelayTable();
        harbourTable.insertTableRow(HarbourInterruptionPage.MENU_CARGO_LOADING_DELAY, dataMap);
        harbourTable.verifyRowWithDataPresent(dataMap);

        //Modify record on screen
        harbourTable.selectRow(dataMap);
        dataMap.put("Comments", "Testing1");
        harbourTable.updateTableRow(harbourTable.getSelectedRow(), dataMap);
        harbourTable.verifyRowWithDataPresent(dataMap);

        //Delete record on screen
        harbourTable.deleteTableRow(HarbourInterruptionPage.MENU_CARGO_LOADING_DELAY, dataMap);
        harbourTable.verifyNoRowPresentWithData(dataMap);
    }
}
