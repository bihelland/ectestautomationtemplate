package com.ec.demo.iud.dbresthelper;

import com.ec.categories.IUD;
import com.ec.frmw.testagent.client.DbRestHelper;
import com.ec.selenium.core.testbase.UITestBase;
import org.apache.log4j.Logger;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Category({ IUD.class })
@RunWith(Arquillian.class)
public class DbRestHelperTest extends UITestBase {

    private static final Logger log = Logger.getLogger(DbRestHelperTest.class);

    @Test
    public void testDbRestHelperScalar() {
        log.info("Step: TestSelectScalar");
        String expected = "NO_SEMICOLON";
        String stmt1 = "SELECT '" + expected + "' FROM dual";
        log.info("--Scalar stmt = " + stmt1);
        String actual = DbRestHelper.executeSqlSelectScalar(stmt1);
        Assert.assertEquals("SelectScalar's call to TestAgent did not return expected value. Expected=[" + expected + "]; Actual=[" + actual + "]", expected, actual);
    }

    @Test
    public void testDbRestHelperScalarSqlNull() {
        log.info("Step: TestSelectScalarSqlNull");
        String expected = ""; //Empty string
        String stmt1 = "SELECT NULL FROM dual WHERE 1=1";//NULL 1=1
        log.info("--Scalar stmt = " + stmt1);

        try {
            String actual = DbRestHelper.executeSqlSelectScalar(stmt1);
            Assert.assertEquals("SelectScalar's call to TestAgent did not return expected value. Expected=[" + expected + "]; Actual=[" + actual + "]", expected, actual);
        } catch (Exception e) {
            log.info("Exception thrown: " + e.getMessage());
            Assert.fail("SelectScalar's call to TestAgent failed due to an exception: " + e.getMessage());
        }
    }

    @Test
    public void testDbRestHelperScalarNoRows() {
        log.info("Step: TestSelectScalarNoRows");
        String expected = ""; //Empty string
        String stmt1 = "SELECT 1 FROM dual WHERE 1=2";//1 1=2
        log.info("--Scalar stmt = " + stmt1);

        try {
            String actual = DbRestHelper.executeSqlSelectScalar(stmt1);
            Assert.assertEquals("SelectScalar's call to TestAgent did not return expected value. Expected=[" + expected + "]; Actual=[" + actual + "]", expected, actual);
        } catch (Exception e) {
            log.info("Exception thrown: " + e.getMessage());
            Assert.fail("SelectScalar's call to TestAgent failed due to an exception: " + e.getMessage());
        }
    }

    @Test
    public void testSelectTable() {
        log.info("Step: TestSelectTable");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("NO_SEMICOLON");
        expected.add("WITH_SEMICOLON");
        String stmt1 = "SELECT '" + expected.get(0) + "' A,'" + expected.get(1) + "' B FROM dual";
        log.info("--Table stmt = " + stmt1);
        List<Map<String,String>> actual = DbRestHelper.executeSqlSelectTable(stmt1);
        Assert.assertEquals("SelectTable's call to TestAgent did not return expected values. Expected_A=[" + expected.get(0) + "]; Actual_A=[" + DbRestHelper.extractValue(actual, 0, "A") + "]", expected.get(0), DbRestHelper.extractValue(actual, 0, "A"));
        Assert.assertEquals("SelectTable's call to TestAgent did not return expected values. Expected_B=[" + expected.get(1) + "]; Actual_B=[" + DbRestHelper.extractValue(actual, 0, "B") + "]", expected.get(1), DbRestHelper.extractValue(actual, 0, "B"));
    }
}
